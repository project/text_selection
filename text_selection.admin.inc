<?php
/**
 * @file
 * Administration UI functions for the Text Selection module.
 */

/**
 * Text selection custom colors applies on every page except the listed pages.
 */
define('TEXT_SELECTION_VISIBILITY_NOTLISTED', 0);

/**
 * Text selection custom colors on only the listed pages.
 */
define('TEXT_SELECTION_VISIBILITY_LISTED', 1);

/**
 * Shows text selection highlight if the associated PHP code returns TRUE.
 */
define('TEXT_SELECTION_VISIBILITY_PHP', 2);

/**
 * Form constructor for the Text Selection Configuration form.
 *
 * @see text_selection_settings_form_validate()
 * @see text_selection_settings_form_submit()
 * @ingroup forms
 */
function text_selection_settings_form() {
  $form['text_selection_bg_color'] = array(
    '#type' => 'textfield',
    '#size' => 7,
    '#maxlength' => 7,
    '#title' => t('Background Color'),
    '#default_value' => variable_get('text_selection_bg_color', '#000000'),
    '#description' => t('Determines the background color for text selection. Format: #000000.'),
  );
  $form['text_selection_font_color'] = array(
    '#type' => 'textfield',
    '#size' => 7,
    '#maxlength' => 7,
    '#title' => t('Font Color'),
    '#default_value' => variable_get('text_selection_font_color', '#FFFFFF'),
    '#description' => t('Determines the font color for text selection. Format: #FFFFFF.'),
  );

  // Fetching values of variables used for visibility and list of pages if set.
  $text_selection_visibility = variable_get('text_selection_visibility', TEXT_SELECTION_VISIBILITY_NOTLISTED);
  $text_selection_pages = variable_get('text_selection_pages', '');

  // Visibility settings.
  $form['visibility_title'] = array(
    '#type' => 'item',
    '#title' => t('Visibility settings'),
  );

  // Per-path visibility.
  $form['visibility']['path'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pages'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'visibility',
    '#weight' => 0,
  );

  $access = user_access('use PHP for settings');

  if (isset($text_selection_visibility) && $text_selection_visibility == TEXT_SELECTION_VISIBILITY_PHP && !$access) {
    $form['visibility']['path']['visibility'] = array(
      '#type' => 'value',
      '#value' => TEXT_SELECTION_VISIBILITY_PHP,
    );
    $form['visibility']['path']['pages'] = array(
      '#type' => 'value',
      '#value' => $text_selection_pages,
    );
  }
  else {
    // Array of visibility options.
    $options = array(
      TEXT_SELECTION_VISIBILITY_NOTLISTED => t('All pages except those listed'),
      TEXT_SELECTION_VISIBILITY_LISTED => t('Only the listed pages'),
    );
    // Defining description and title for visibility settings.
    $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));

    if (module_exists('php') && $access) {

      $options += array(TEXT_SELECTION_VISIBILITY_PHP => t('Pages on which this PHP code returns <code>TRUE</code> (experts only)'));
      $title = t('Pages or PHP code');
      $description .= ' ' . t('If the PHP option is chosen, enter PHP code between %php. Note that executing incorrect PHP code can break your Drupal site.', array('%php' => '<?php ?>'));
    }
    else {
      $title = t('Pages');
    }
  }

  $form['visibility']['path']['text_selection_visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Apply text selection on specific pages'),
    '#options' => $options,
    '#default_value' => $text_selection_visibility,
  );

  $form['visibility']['path']['text_selection_pages'] = array(
    '#type' => 'textarea',
    '#title' => '<span class="element-invisible">' . $title . '</span>',
    '#default_value' => $text_selection_pages,
    '#description' => $description,
  );

  return system_settings_form($form);
}

/**
 * Form validation handler for text_selection_settings_form().
 *
 * @see text_selection_settings_form()
 * @see text_selection_settings_form_submit()
 */
function text_selection_settings_form_validate($form, &$form_state) {
  if (!empty($form_state['values']['text_selection_bg_color'])) {
    $bg_color = $form_state['values']['text_selection_bg_color'];
    if (!preg_match("/^(#)([0-9a-fA-F]{3})([0-9a-fA-F]{3})?$/", $bg_color)) {
      form_set_error('text_selection_bg_color', t('The background color must fit the hexadecimal HTML color default format.'));
    }
  }
  else {
    form_set_error('text_selection_bg_color', t('The background color value is missing.'));
  }
  if (!empty($form_state['values']['text_selection_font_color'])) {
    $font_color = $form_state['values']['text_selection_font_color'];
    if (!preg_match("/^(#)([0-9a-fA-F]{3})([0-9a-fA-F]{3})?$/", $font_color)) {
      form_set_error('text_selection_font_color', t('The font color must fit the hexadecimal HTML color default format.'));
    }
  }
  else {
    form_set_error('text_selection_font_color', t('The font color value is missing.'));
  }
}

/**
 * Form submission handler for text_selection_settings_form().
 *
 * Saves the text selection color values.
 *
 * @see text_selection_settings_form()
 * @see text_selection_settings_form_validate()
 */
function text_selection_settings_form_submit($form, &$form_state) {
  variable_set('text_selection_bg_color', $form_state['values']['text_selection_bg_color']);
  variable_set('text_selection_font_color', $form_state['values']['text_selection_font_color']);
}
